import React, { useState, useEffect } from 'react';

interface Item {
    name: string;
    mass: number;
    height: number;
}

export default function PeopleList() {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [items, setItems] = useState([]);

    // Примечание: пустой массив зависимостей [] означает, что
    // этот useEffect будет запущен один раз
    // аналогично componentDidMount()
    useEffect(()=>{
        const fetchData = async () => {
            try {
                const response = await fetch("https://swapi.dev/api/people/");
                const data = await response.json();
    
                setIsLoaded(true);
                setItems(data.results);
            }
            catch (error) {
                // Примечание: Обрабатывать ошибки необходимо именно здесь
                setIsLoaded(true);
                setError(error);
            }
        }

        fetchData();
    }, [])

    if (error) {
        return <div>Ошибка: {error}</div>;
    }

    if (!isLoaded) {
        return <div>Загрузка....</div>;
    }

    return (
        <table>
            <tr>
                <th>Name</th>
                <th>Height</th>
                <th>Mass</th>
            </tr>
            {items.map((item:Item) => (
                <tr key={item.name}>
                    <td>{item.name}</td>
                    <td>{item.height}</td>
                    <td>{item.mass}</td>
                </tr>
            ))}

        </table>
    );
}