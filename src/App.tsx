import React from 'react';
import PeopleList from './PeopleList';

export default function App() {
  return (
  <div>
    <h1>Home</h1>
    <PeopleList />
  </div>)
}